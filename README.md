OpenNMS AMQP Processor for OnGuard Appliance Alarms
===================================================
OpenNMS contains an AMQP Alarm Northbounder which can optionally be further
processed by a custom Camel processor. This is that, for converting from
NorthboundAlarm to the XML format used for OnGuard alarms.

Requirements
============
* OpenNMS new enough to include AMQP Northbounder. 
* AMQP broker.

Usage
=====
See OpenNMS docs.

Testing
=======
TODO: explain local broker requirements for running integration test.
An integration test exists that requires a local AMQP broker. Use the QPID C++ broker for best results.
This test is skipped by default. To run:

```
mvn package -DskipTests=false
```

License
=======


amqp-opennms-alarm is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

amqp-opennms-alarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with amqp-opennms-alarm.  If not, see:
     http://www.gnu.org/licenses/

See COPYING file for full license.
