package com.convergeone.amqp.processor

import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.opennms.netmgt.alarmd.api.NorthboundAlarm
import org.opennms.netmgt.model.OnmsSeverity
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static java.lang.Boolean.TRUE
import static org.apache.camel.Exchange.ROUTE_STOP
import static org.opennms.netmgt.model.OnmsSeverity.CLEARED

/**
 * Created by isiamionau on 4/20/2017.
 */
class NorthboundAlarmProcessor implements Processor {
    private static final Logger LOG = LoggerFactory.getLogger(NorthboundAlarmProcessor.class)
    private static String DELIMITER = ":"
    private static String DEVICE_URL = "127.0.0.1"

    void process(Exchange exchange) throws Exception {
        NorthboundAlarm northboundAlarm = exchange.getIn().getBody(NorthboundAlarm.class)
        if (northboundAlarm.getCount() != 1 || northboundAlarm.severity == CLEARED) {
            LOG.debug("The alarm is not sending to QPID, because the one is a duplicate or the severity type is CLEARED: {}", northboundAlarm)
            exchange.setProperty(ROUTE_STOP, TRUE)
            return
        }
        exchange.getIn().setBody(getAlarmTransformed(northboundAlarm), String.class)
    }

    /**
     * Marshal {@link org.opennms.netmgt.alarmd.api.NorthboundAlarm} to an OEP northboundAlarm
     * @param northboundAlarm gotten from OpenNMS
     * @return an alarm suitable for OEP
     */
    @SuppressWarnings("GroovyAssignabilityCheck")
    static String getAlarmTransformed(NorthboundAlarm northboundAlarm) {
        def builder = new StreamingMarkupBuilder()
        builder.encoding = 'UTF-8'
        def timestamp = (northboundAlarm.firstOccurrence != null ? northboundAlarm.firstOccurrence : new java.util.Date())
        def nodeLabel = northboundAlarm.nodeLabel
        def finder = nodeLabel =~ /^(?:(.*?)-)?((cc\d{4})-.*?)(?:-([ab]))?.onguard.convergeone.com$/
        def client = 'CC1237'
        def site = 'CC1237-PRIMARY'
        def ha = null
        if (finder.matches()) {
            site = finder.group(2).toUpperCase()
            client = finder.group(3).toUpperCase()
            ha = finder.group(4)
        }
        def applianceId
        if (ha != null) {
            applianceId = (site + '-' + ha).toUpperCase()
        } else {
            applianceId = site.toUpperCase()
        }
        def xml = builder.bind {
            mkp.xmlDeclaration()
            Alarm(AlarmID: site + DELIMITER + northboundAlarm.getId(), DeviceID: client + DELIMITER + DEVICE_URL, TimeStamp: timestamp) {
                clientid client
                applianceid applianceId
                description { mkp.yieldUnescaped("<![CDATA[${northboundAlarm.desc}]]>") }
                uei northboundAlarm.uei
                ipaddr northboundAlarm.ipAddr
                if (northboundAlarm.eventParametersCollection != null && northboundAlarm.eventParametersCollection.size() > 0) {
                    eventParms {
                        northboundAlarm.eventParametersCollection.each { p ->
                            eventParm {
                                parmName p.getName()
                                parmValue p.getValue()
                            }
                        }
                    }
                }
                'AssetName' northboundAlarm.nodeLabel
                'Type' northboundAlarm.severity
                'Code' northboundAlarm.alarmKey
                if (northboundAlarm.clearKey != null) {
                    'ClearKey' northboundAlarm.clearKey
                }
                source 'APPLIANCE_MONITOR'
            }
        }
        String serialize = XmlUtil.serialize(xml)
        LOG.debug("There is an northboundAlarm {}", serialize)
        return serialize
    }

}
