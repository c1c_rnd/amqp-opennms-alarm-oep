package com.convergeone.amqp.test

import org.opennms.netmgt.alarmd.api.NorthboundAlarm
import org.opennms.netmgt.model.OnmsEventParameter
import org.opennms.netmgt.model.OnmsSeverity
import org.opennms.netmgt.xml.event.Parm
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Shared
import spock.lang.Specification

import static com.convergeone.amqp.processor.NorthboundAlarmProcessor.getAlarmTransformed

/**
 * Created by isiamionau on 6/8/2017.
 */
class CreateXmlFromAlarmSpec extends Specification {
    static final Logger LOGGER = LoggerFactory.getLogger(CreateXmlFromAlarmSpec.class)

    @Shared
    NorthboundAlarm alarm

    def setupSpec() {
        alarm = new NorthboundAlarm()
        alarm.firstOccurrence = new Date(1498677804209)
        alarm.id = 13123
        alarm.desc = '<p>second alarm for this device</p>'
        alarm.alarmKey = 'uei.opennms.org/internal/importer/importSuccessful:dns://localhost/pe.spanlink.com/watch?expression=^coremonitor.*'
        alarm.clearKey = 'uei.opennms.org/nodes/nodeDfdsfown:CC1237:1003:c1ctest0001S'
        alarm.severity = OnmsSeverity.CLEARED
        List<OnmsEventParameter> m_eventParametersCollection = new ArrayList();
        alarm.eventParametersCollection = m_eventParametersCollection
        m_eventParametersCollection.add(new OnmsEventParameter(new Parm("param1", "value1")))
        m_eventParametersCollection.add(new OnmsEventParameter(new Parm("importRescanExisting", "true")))
        m_eventParametersCollection.add(new OnmsEventParameter(new Parm("importStats", "Deletes: 0, Updates: 0, Inserts: 0" +
                "Importing: has not begun, Loading: has not begun, Auditing: has not begun" +
                "Scanning: has not begun, Processing: has not begun, Relating: has not begun" +
                "Total Scan Effort: 0.0 thread-seconds, Total Write Effort: 0.0 thread-seconds, Total Event Sending Effort: 0.0 thread-seconds")))
    }

    def 'Test transform an alarm to XML: nodeLabel = null'() {
        when: "a new NorthboundAlarm class is created and processed"
        String s = getAlarmTransformed(alarm)
        String date = new Date(1498677804209).toString()
        then:
        s.replace('\r','').replace('\n','') == "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Alarm AlarmID=\"CC1237-PRIMARY:13123\" DeviceID=\"CC1237:127.0.0.1\" TimeStamp=\"" + date + "\">" +
                "  <clientid>CC1237</clientid>" +
                "  <applianceid>CC1237-PRIMARY</applianceid>" +
                "  <description><![CDATA[<p>second alarm for this device</p>]]></description>" +
                "  <uei/>" +
                "  <ipaddr/>" +
                "  <eventParms>" +
                "    <eventParm>" +
                "      <parmName>param1</parmName>" +
                "      <parmValue>value1</parmValue>" +
                "    </eventParm>" +
                "    <eventParm>" +
                "      <parmName>importRescanExisting</parmName>" +
                "      <parmValue>true</parmValue>" +
                "    </eventParm>" +
                "    <eventParm>" +
                "      <parmName>importStats</parmName>" +
                "      <parmValue>Deletes: 0, Updates: 0, Inserts: 0" +
                "Importing: has not begun, Loading: has not begun, Auditing: has not begun" +
                "Scanning: has not begun, Processing: has not begun, Relating: has not begun" +
                "Total Scan Effort: 0.0 thread-seconds, Total Write Effort: 0.0 thread-seconds, Total Event Sending Effort: 0.0 thread-seconds</parmValue>" +
                "    </eventParm>" +
                "  </eventParms>" +
                "  <AssetName/>" +
                "  <Type>CLEARED</Type>" +
                "  <Code>uei.opennms.org/internal/importer/importSuccessful:dns://localhost/pe.spanlink.com/watch?expression=^coremonitor.*</Code>" +
                "  <ClearKey>uei.opennms.org/nodes/nodeDfdsfown:CC1237:1003:c1ctest0001S</ClearKey>" +
                "  <source>APPLIANCE_MONITOR</source>" +
                "</Alarm>"
    }

    def 'Test transform an alarm to XML nodeLabel = watch-cc4010-primary-a.onguard.convergeone.com'() {
        when: "a new NorthboundAlarm class is created and processed"
        alarm.nodeLabel ='watch-cc4010-primary-a.onguard.convergeone.com'
        String s = getAlarmTransformed(alarm)
        String date = new Date(1498677804209).toString()
        then:
        s.replace('\r','').replace('\n','') == "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Alarm AlarmID=\"CC4010-PRIMARY:13123\" DeviceID=\"CC4010:127.0.0.1\" TimeStamp=\"" + date + "\">" +
                "  <clientid>CC4010</clientid>" +
                "  <applianceid>CC4010-PRIMARY-A</applianceid>" +
                "  <description><![CDATA[<p>second alarm for this device</p>]]></description>" +
                "  <uei/>" +
                "  <ipaddr/>" +
                "  <eventParms>" +
                "    <eventParm>" +
                "      <parmName>param1</parmName>" +
                "      <parmValue>value1</parmValue>" +
                "    </eventParm>" +
                "    <eventParm>" +
                "      <parmName>importRescanExisting</parmName>" +
                "      <parmValue>true</parmValue>" +
                "    </eventParm>" +
                "    <eventParm>" +
                "      <parmName>importStats</parmName>" +
                "      <parmValue>Deletes: 0, Updates: 0, Inserts: 0" +
                "Importing: has not begun, Loading: has not begun, Auditing: has not begun" +
                "Scanning: has not begun, Processing: has not begun, Relating: has not begun" +
                "Total Scan Effort: 0.0 thread-seconds, Total Write Effort: 0.0 thread-seconds, Total Event Sending Effort: 0.0 thread-seconds</parmValue>" +
                "    </eventParm>" +
                "  </eventParms>" +
                "  <AssetName>watch-cc4010-primary-a.onguard.convergeone.com</AssetName>" +
                "  <Type>CLEARED</Type>" +
                "  <Code>uei.opennms.org/internal/importer/importSuccessful:dns://localhost/pe.spanlink.com/watch?expression=^coremonitor.*</Code>" +
                "  <ClearKey>uei.opennms.org/nodes/nodeDfdsfown:CC1237:1003:c1ctest0001S</ClearKey>" +
                "  <source>APPLIANCE_MONITOR</source>" +
                "</Alarm>"
    }

    def 'Test transform an alarm to XML nodeLabel = cc4010-primary-a.onguard.convergeone.com'() {
        when: "a new NorthboundAlarm class is created and processed"
        alarm.nodeLabel ='cc4010-primary-a.onguard.convergeone.com'
        String s = getAlarmTransformed(alarm)
        String date = new Date(1498677804209).toString()
        then:
        s.replace('\r','').replace('\n','') == "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Alarm AlarmID=\"CC4010-PRIMARY:13123\" DeviceID=\"CC4010:127.0.0.1\" TimeStamp=\"" + date + "\">" +
                "  <clientid>CC4010</clientid>" +
                "  <applianceid>CC4010-PRIMARY-A</applianceid>" +
                "  <description><![CDATA[<p>second alarm for this device</p>]]></description>" +
                "  <uei/>" +
                "  <ipaddr/>" +
                "  <eventParms>" +
                "    <eventParm>" +
                "      <parmName>param1</parmName>" +
                "      <parmValue>value1</parmValue>" +
                "    </eventParm>" +
                "    <eventParm>" +
                "      <parmName>importRescanExisting</parmName>" +
                "      <parmValue>true</parmValue>" +
                "    </eventParm>" +
                "    <eventParm>" +
                "      <parmName>importStats</parmName>" +
                "      <parmValue>Deletes: 0, Updates: 0, Inserts: 0" +
                "Importing: has not begun, Loading: has not begun, Auditing: has not begun" +
                "Scanning: has not begun, Processing: has not begun, Relating: has not begun" +
                "Total Scan Effort: 0.0 thread-seconds, Total Write Effort: 0.0 thread-seconds, Total Event Sending Effort: 0.0 thread-seconds</parmValue>" +
                "    </eventParm>" +
                "  </eventParms>" +
                "  <AssetName>cc4010-primary-a.onguard.convergeone.com</AssetName>" +
                "  <Type>CLEARED</Type>" +
                "  <Code>uei.opennms.org/internal/importer/importSuccessful:dns://localhost/pe.spanlink.com/watch?expression=^coremonitor.*</Code>" +
                "  <ClearKey>uei.opennms.org/nodes/nodeDfdsfown:CC1237:1003:c1ctest0001S</ClearKey>" +
                "  <source>APPLIANCE_MONITOR</source>" +
                "</Alarm>"
    }

    def 'Test transform an alarm to XML nodeLabel = cc4010-primary.onguard.convergeone.com'() {
        when: "a new NorthboundAlarm class is created and processed"
        alarm.nodeLabel ='cc4010-primary.onguard.convergeone.com'
        String s = getAlarmTransformed(alarm)
        String date = new Date(1498677804209).toString()
        then:
        s.replace('\r','').replace('\n','') == "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Alarm AlarmID=\"CC4010-PRIMARY:13123\" DeviceID=\"CC4010:127.0.0.1\" TimeStamp=\"" + date + "\">" +
                "  <clientid>CC4010</clientid>" +
                "  <applianceid>CC4010-PRIMARY</applianceid>" +
                "  <description><![CDATA[<p>second alarm for this device</p>]]></description>" +
                "  <uei/>" +
                "  <ipaddr/>" +
                "  <eventParms>" +
                "    <eventParm>" +
                "      <parmName>param1</parmName>" +
                "      <parmValue>value1</parmValue>" +
                "    </eventParm>" +
                "    <eventParm>" +
                "      <parmName>importRescanExisting</parmName>" +
                "      <parmValue>true</parmValue>" +
                "    </eventParm>" +
                "    <eventParm>" +
                "      <parmName>importStats</parmName>" +
                "      <parmValue>Deletes: 0, Updates: 0, Inserts: 0" +
                "Importing: has not begun, Loading: has not begun, Auditing: has not begun" +
                "Scanning: has not begun, Processing: has not begun, Relating: has not begun" +
                "Total Scan Effort: 0.0 thread-seconds, Total Write Effort: 0.0 thread-seconds, Total Event Sending Effort: 0.0 thread-seconds</parmValue>" +
                "    </eventParm>" +
                "  </eventParms>" +
                "  <AssetName>cc4010-primary.onguard.convergeone.com</AssetName>" +
                "  <Type>CLEARED</Type>" +
                "  <Code>uei.opennms.org/internal/importer/importSuccessful:dns://localhost/pe.spanlink.com/watch?expression=^coremonitor.*</Code>" +
                "  <ClearKey>uei.opennms.org/nodes/nodeDfdsfown:CC1237:1003:c1ctest0001S</ClearKey>" +
                "  <source>APPLIANCE_MONITOR</source>" +
                "</Alarm>"
    }
}
